package exemplo;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;

import domain.Modelo;
import domain.Pessoa;
import domain.Veiculo;
import enums.TipoVeiculoEnum;

public class Exemplo1 {

	
		public static void main(String[] args)  {
			EntityManagerFactory emf = null;
			try {
				emf = Persistence.createEntityManagerFactory("avaliacao2");
				EntityManager em = emf.createEntityManager();
				em.getTransaction().begin();
				Modelo m1 = new Modelo("Uno Fire");
				em.persist(m1);
				Pessoa p1 = new Pessoa("07712682552", "Rafael", "rembrandtx@live.com");
				em.persist(p1);
				Veiculo v1 = new Veiculo("JRS9384", "12345678912345678", m1, TipoVeiculoEnum.PASSEIO, new Date("03/06/2019"), p1);
				em.persist(v1);
				em.getTransaction().commit();
				
//				//CONSULTA 1
//				TypedQuery<Veiculo> query = em.createQuery("select v from tab_veiculo v where v.modelo.nome = :modelo",Veiculo.class);
//				query.setParameter("modelo", "Uno Fire");
//				List<Veiculo> veiculos = query.getResultList();
//				System.out.println("Compradores: ");
//				for (Veiculo veiculo : veiculos) {		
//					System.out.println(veiculo.getComprador().getNome() + ": " + veiculo.getModelo().getNome());
//				}
				
//				//CONSULTA 2
//				TypedQuery<Veiculo> query3 = em.createQuery("select v.veiculo.tipo, count(*) from tab_veiculo v where v.veiculo.tipo= :tipo and v.comprador.nome = :comprador group by v.veiculo.tipo ",Veiculo.class);
//				query3.setParameter("tipo", "1");
//				String comprador = "Rafael";
//				query3.setParameter("comprador", comprador);
//				List<Veiculo> veiculos2 = query3.getResultList();
//				System.out.println("QTD de veiculos:");
//				for (Veiculo veiculo : veiculos2) {
//					System.out.println("");
//				}
				
//				//CONSULTA 3
//				TypedQuery<Veiculo> query2 = em.createQuery("select v from tab_veiculo v where v.modelo.nome = :modelo and v.comprador.nome = :comprador",Veiculo.class);
//				query2.setParameter("modelo", "Uno Fire");
//				String comprador = "Rafael";
//				query2.setParameter("comprador", comprador);
//				List<Veiculo> veiculos2 = query2.getResultList();
//				System.out.println("Veiculos de : " + comprador );
//				for (Veiculo veiculo2 : veiculos2) {		
//					System.out.println(veiculo2.getComprador().getNome() + ": " + veiculo2.getModelo().getNome() + "-> placa:" + veiculo2.getPlaca());
//				}
				
				
				
			} catch (Exception e) {
				logger.error("Erro: ", e);
			} finally {
				if (emf != null) {
					emf.close();
				}
			}
		}
		private static final Logger logger = Logger.getLogger(Exemplo1.class);
	}
	
