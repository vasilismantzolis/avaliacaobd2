package enums;

public enum TipoVeiculoEnum {
		CARGA("CR"), PASSEIO("PS");
		
	private String tipo;

	private TipoVeiculoEnum(String tipo) {
		this.tipo = tipo;
	}

	public String getCodigo() {
		return tipo;
	}

	public static TipoVeiculoEnum valueOfCodigo(String tipo) {
		for (TipoVeiculoEnum tipoVeiculoEnum : values()) {
			if (tipoVeiculoEnum.getCodigo().equalsIgnoreCase(tipo)) {
				return tipoVeiculoEnum;
			}
		}
		throw new IllegalArgumentException("Codigo n valido: " + tipo);
	}
 
		
}
